{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "id": "fbNXzKJY9W_8",
    "jupyter": {
     "outputs_hidden": false,
     "source_hidden": false
    },
    "nteract": {
     "transient": {
      "deleting": false
     }
    }
   },
   "source": [
    "# Getting started"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Run this example in* [colab](https://colab.research.google.com/drive/16tEQBo8GzqehT8fRoiuTA7uHF7zyIRZ4)!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "pJqgVMV09W_-"
   },
   "source": [
    "To use MICE, first you need to define a function with the gradient that you want to approximate.\n",
    "For example, here we have a two-dimensional quadratic function,\n",
    "\n",
    "$$\n",
    "f(\\boldsymbol{\\xi}, \\theta) = \\frac{1}{2} \\boldsymbol{\\xi} \\cdot \\boldsymbol{H}(\\theta) \\boldsymbol{\\xi}\n",
    "- \\boldsymbol{b} \\cdot \\boldsymbol{\\xi}\n",
    "$$\n",
    "\n",
    "where\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "    \\boldsymbol{\\xi} \\quad & \\textrm{ is a vector containing the optimization variables, } \\\\\n",
    "    \\theta \\quad & \\textrm{ is a random variable, } \\\\\n",
    "    f(\\boldsymbol{\\xi}, \\theta) \\quad & \\textrm{ is a function whose expectation wrt } \\theta \\textrm{ is to be minimized, } \\\\\n",
    "    \\boldsymbol{H}(\\theta) \\quad & \\textrm{ is the Hessian matrix, } \\\\\n",
    "    \\boldsymbol{b} \\quad & \\textrm{is a vector of the same dimension as } \\boldsymbol{\\xi}.\n",
    "    \\end{aligned}\n",
    "$$\n",
    "\n",
    "Here, we have a Hessian that is a convex combination of the identity matrix and of an arbitrary matrix:\n",
    "\n",
    "$$\n",
    "\\boldsymbol{H}(\\theta) =\n",
    "\\boldsymbol{I}_2(1 -\\theta) +\n",
    "\\begin{bmatrix}\n",
    "\\kappa & \\kappa-1 \\\\\n",
    "        \\kappa-1 & \\kappa\n",
    "\\end{bmatrix}\n",
    "\\theta,\n",
    "$$\n",
    "\n",
    "noting that $\\theta$ is a random variable such that $\\theta \\sim \\mathcal{U}[0, 1]$\n",
    "and $\\kappa$ is the desired conditioning number of the expected value of the Hessian.\n",
    "Also, $\\boldsymbol{b}$ is a 2-dimensional vector with ones.\n",
    "\n",
    "To use the stochastic gradient method, we need the gradient of $f$ wrt $\\boldsymbol{\\xi}$:\n",
    "\n",
    "$$\n",
    "\\nabla_{\\boldsymbol{\\xi}} f(\\boldsymbol{\\xi}, \\theta) = \\boldsymbol{H}(\\theta) \\boldsymbol{\\xi} - \\boldsymbol{b}.\n",
    "$$\n",
    "\n",
    "To use MICE to estimate $\\nabla_{\\boldsymbol{\\xi}} f$, we need to import the MICE class and NumPy to assist us in\n",
    "defining our problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "Oyf1IPOy9XAA",
    "outputId": "4eb99b6a-d70f-4d46-e43c-7f896cc6e30c",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "!pip install -U mice\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from mice import MICE, plot_mice"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "id": "0VVXplSp9XAC",
    "jupyter": {
     "outputs_hidden": false,
     "source_hidden": false
    },
    "nteract": {
     "transient": {
      "deleting": false
     }
    }
   },
   "source": [
    "Then, we need to define $\\nabla_{\\boldsymbol{\\xi}} f$ as a function of $\\boldsymbol{\\xi}$ and an array representing a sample of $\\theta$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "id": "krCvP3Zc9XAC"
   },
   "outputs": [],
   "source": [
    "kappa = 100\n",
    "H_aux = np.array([[kappa, kappa-1], [kappa-1, kappa]])\n",
    "b = np.ones(2)\n",
    "\n",
    "\n",
    "def dobjf(x, thetas):\n",
    "    gradients = []\n",
    "    for theta in thetas:\n",
    "        H = np.eye(2) * (1 - theta) + H_aux * theta\n",
    "        gradients.append(H @ x.T - b)\n",
    "    return np.vstack(gradients)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "q__y_Ynp9XAD"
   },
   "source": [
    "As for $\\theta$, we can pass to MICE either a sampler function that takes the desired sample size as input and returns the sample\n",
    "or a list/array containing the data.\n",
    "Here, we will define a function that returns the uniform sample between 0 and 1,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "id": "CdHcRIwv9XAE"
   },
   "outputs": [],
   "source": [
    "def sampler(n):\n",
    "    return np.random.uniform(0, 1, int(n))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "yzrlqACd9XAF"
   },
   "source": [
    "Now, let's create an instance of MICE to solve this optimization problem with tolerance to statistical error of $\\epsilon=0.7$,\n",
    "maximum cost of $10,000$ evaluations of $\\nabla_{\\boldsymbol{\\xi}} f$, and a minimum batch size of $5$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "id": "neQcr2Kp9XAG"
   },
   "outputs": [],
   "source": [
    "df = MICE(dobjf,\n",
    "          sampler=sampler,\n",
    "          eps=.7,\n",
    "          max_cost=1e4,\n",
    "          min_batch=5,\n",
    "          stop_crit_norm=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "fL6k8kcg9XAH"
   },
   "source": [
    "To perform optimization, we need to set a starting point and a step size. \n",
    "Here, we know both the $L$-smoothness and the $\\mu$-convexity parameters of the problem, thus we can set the step size optimally."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "id": "oOzTl4WS9XAI"
   },
   "outputs": [],
   "source": [
    "x = np.array([20., 50.])\n",
    "L = kappa\n",
    "mu = 1\n",
    "step_size = 2 / (L + mu) / (1 + df.eps ** 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "tFXwv_Ez9XAI"
   },
   "source": [
    "and, finally, we iterate until MICE's cost is reached, in which case df.terminate returns True,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "id": "yLfbFvi_9XAJ"
   },
   "outputs": [],
   "source": [
    "while True:\n",
    "    grad = df.evaluate(x)\n",
    "    if df.terminate:\n",
    "        break\n",
    "    x = x - step_size * grad"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "mM_n5leA9XAJ"
   },
   "source": [
    "Finally, let's plot the convergence of the norm of MICE's gradient estimates from the log DataFrame using the built-in plot_mice function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 354
    },
    "id": "gnRLzwsH9XAK",
    "outputId": "4fb2a412-3986-4bb7-d02e-0ca4b273715c"
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<matplotlib.legend.Legend at 0x7f7fd1b3ef40>"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYkAAAE+CAYAAAB4EuxwAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4yLjEsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy+j8jraAAAgAElEQVR4nOy9e3xcZZ34/34yuTRtCtOmF3pBQ1pSii2EpAVRuQQS/SLURWmoF9hVVxLdiz8XMLGIu/sFVBLgu353V92UVdjFrxBSXBSwYtI0u+wqJZdGW60NTYi0TdrStENzvz6/P2bO6ZlcmkzOzJyZfM779ZpXZ86Zc87znvP0fPLcldYaFxcXFxeXyUhwOgEuLi4uLrGLGyRcXFxcXKbEDRIuLi4uLlPiBgkXFxcXlylxg4SLi4uLy5S4QcLFxcXFZUoSnU5AKCiltgBbgHucTouLi4tLPKK1VqF8X8XjOAmllJ5Nuuvq6rjxxhvDn6A4QLI7uP6S/SW7Q7C/UsoNEueju7ubhQsXRiBFsY9kd3D9JftLdodg/9kECVFtEo2NjU4nwTEku4PrL9lfsjvY94/XNolZkZqaGsbUxBeS3cH1l+wv2R3s+8dVSUJr/ZLWumi2x2dkZIQxNfGFZHdw/SX7S3YH+/5xVZKwy8GDB1m+fLnTyXAEye4QPf+xsTFOnTqFz+djdHQ04tebKQMDA5w+fdrpZDiCJHePx4PX62XJkiUkJPjLAHbzvqggIfkvCsnuED3/o0ePopQiIyODpKQklAqpjTBiDA4OkpKS4nQyHEGKu9aa4eFhTpw4wdGjR3nPe94D2M/7cVXdpJTaopTaMdvju7u7w5mcuEKyO0TPv7e3l1WrVpGcnBwzAQKIqVJNtJHirpQiOTmZVatW0dvba263m/fjKkjYbZPo6uoKZ3LiCsnuEF1/o5gfS0h5UE6GNPfx+c9u3hdT3XTRRXDixI1B25Yvh+PHnUlPtMnNzXU6CY4i3X/+/PlOJ8ExJLuD/bwfe3/yRIgTJ2a2ba7i9hWX7d/X1+d0EhxDsjvYz/tigoR00tLSnE6Co0j3N6ogdu7cyZo1a8jNzSU3N5empiaHUxZ5YrH6L5rYzftiqpuks3LlSqeT4CjS/ZOSkmhra6O0tJTGxka8Xi8+ny+oa2hbWxuZmZkhnXc2x0SbpKQkp5PgKHbzflyFWLu9myTT0tLidBIcJR78L7oIlAp+XXRReM49ODhIW1sbOTk5eL1eALxeb9ADvrCwMOTzzuaYaDM4OOh0EhzFbt6PqyBhp3fTZGNJJI0tW7NmjdNJcJR48I9ku1lKSgqbNm2ipqaG8vJy2tragvaXlpbS1NREQUEB5eXlABQUFFBQUEBubi4+nw+fz2fuLywsnPSYWETCGInzYTvva63j7uVPduj89Q/r9Ppv7JrVsfHOvn37nE6Co0TL//e///2sj4XJX+Ggt7dXa611a2ur3rp1qwZ0fn6+PnPmjPmdnJycSY+tqKjQRUVF+syZMxrQZWVl0x4TSxjukrDmQ2veDzw7Q3rexlVJwi5qZJC+oVHGxuJvenS7+Hw+p5PgKNL9jbECmZmZVFVVobUmMzOT0tLSKY9pa2tjx44dVFdXm20XXq+XkpKSqKQ5XEgbJzEeu3lfVJBYm7EagL5heZlG+jgB6f6TjRUoLS2loaFh0u83NTVRWFjIpk2bKC4uNrcvXrw4YmmMFO44CXecxIw51XkMgN7BEYdTEn2kjxOIB/9Itpv19fVRU1NDTU2Nua2iooL8/Pyg7xl/dTY0NLBt2zZycnKC2i+MRu/JjolV3HESgsZJ2O3dtPiCBQD0CAwSk/3nlkQ8+B8/PrFFIlwzAng8HrOqac2aNaxZswafz0dZWZn5nfz8fG6++WaKi4u58847qaiooKCggOrq6ilLENZjYhWPx+N0EhzFbt4XtXzpc//1O77283Z+9lcf5IrVsf/QCCdHjhzh4osvdjoZjhEt/4MHD7J+/fqIXydUhoaGSE5OdjoZjiDR3ZoPrXnfXb50Gs684/+zrHdQXptEa2ur00lwFOn+kscKSHYH+3lfVJDIynwvILNNIisry+kkOIp0f8ljBSS7g/28LypI9PhOAdA7JC9IdHR0OJ0ER5HuPzw87HQSHEOyO9jP+6KCxNigv5eDxOqmnp4ep5PgKNL9x8bGnE6CY0h2B/t5X1SQeP+mqwCZ1U3SxwlI95c8VkCyO7jjJELi4P5mQGYX2HgYJxBJpPtLHisg2R3s5/24mipcKbUF2DLb45cuWUJq0jGRJYn09HSnk+Ao0v0ljxWQ7A72835clSS0zTWuFy5cyIKURHqH5LVJLFy40OkkOIp0f8kPSsnuYD/vx1WQsEt7eztpKR6RJYn29nank+Ao0v2HhoZYtGgRBQUFFBYWUlBQEFOjpCM5lfvQ0FDEzh0P2M37cVXdZJf169ez4LctIoNELI4CjibS/efNmwdAVVWVOU3Djh07yM3NnfPtNYa7VOzmfXEliQXJiSIbrqX/JS3df7JRx0VFRSxevDho0r+5iPQR13bzvqgg0d/fz4IUD30C2yT6+/udToKjSPefaq6zwsJCqqqqJqw6B/6ShrE6nRFIfD4fubm5FBcXs2bNGrPKaqrtBuXl5eTm5lJQUGDOGltcXGweY11rO9zE4/x04cR23g91laJYeDHL5brOnj2r//L/Neq8x/bM6vh45uzZs04nwVGi5W9nZTqttS4rK9O1tbW6trY2aAU4u4yMjGiv1xu0Ep3WWldXV5sr1GFZda6xsVFv3brV/F5OTo4+c+aMPnPmjPZ6veb2zMxMXV1dPeV241z5+flaa//KeEVFReZ1DazHhpuRkZGInTtWseZDa97HXZnu/DQ2Noqtbprr9c7TES/+mzdv5qabbuKmm25i8+bNYTvvVGMF2trayMzMBIJXnauoqGDbtm3m9/Lz883ShHXa8OLiYpqams67vbKyktOnT5vrYjc0NFBdXW2WWMYfG27ccRKCxknY5UNbt3JjV5f/w4OBjcuXh2/S/hhm2bJlTifBUaT7JyZO/l+9urraDAbTPagnqxLyer2TLjo0fvv27dvZunWr+fl8y6aGm6ncpWA374sqSSQaAcLKiRPRT4gDSJtPfzzx4l9fX09tbS21tbXU19eH7bxKTVxCYOfOnbS1tZkPb+viNIWFhVRUVJifa2pqzFXsrMHCurrdVNu3bdsWdK62tjYKCgqoqqoC/O0ZkWyTmMxdEnbzvuwQK4ijR4+ydu1ap5PhGPHib1T3AOTl5YXtvMZMqPfccw/gfzBnZmZOWRWRn59PU1OTOe/P9u3byczMNEsHhYWFtLW1kZ+fT05OzpTbAXJycsyxGQAFBQWUlJRQVVVFbm4umzZtMqu8IsHw8LDobrB2876olemY6i+KOPwNQuXUqVMsWbLE6WQ4RrT8Y3VluuHhYZKSkmyfx+jFNH4hm6m2xwLhco8nrPnQmvfn/Mp0dte4lkxLS4vTSXAU6f6SxwpIdgf7eT+ugoS2OXfT0GQNc8uX20lS3DA6Km9siBXp/vFYYxAuJLuD/bwfV0HCLn2trbz8m2NklL7Moc6z/momAT2bADZu3Oh0EhxFun9qampYzuP1eietUppqeywQLvd4xW7eFxUkmpubmZfonxFycETWX5bNzc1OJ8FRpPtLHnEu2R3s531RQWLFihXMS/IHiYFhWUsarlixwukkOIp0f2kNt1Yku4P9vC8qSADMS/IrDwzLKkm4uLi4zAZRQaKzs5OURKMkIStIdHZ2Op0ER5Hub4yTkIhkd7Cf90UFiezsbLMkMTgiq7opOzvb6SQ4inR/yY23kt3Bft4XFST2799vaZOQVZLYv3+/00lwFOn+khtvJbuD/bwvKkh4PB5SjDYJYSUJ6ev8SveXPH+RZHewn/dFBYmsrCyzTWJQWEkiKyvL6SQ4inT/lJQU1qxZEzQ9t0FpaWnQg3TRokVBM7g2NTVRUFBgLhpUXl5ufs/YZryM6cFjiZSUFKeT4Ch2876oCf4OHDjAtR+6DpDXJnHgwAFuvPFGp5PhGPHgPzo2yq7Du9jXuY+rVlzFLWtvwZMQnhLQwMAA4J+BdTw1NTVBM8BaaWpqorCwkOrqanMSPus5du/ePeWxscLAwIDobrB2876oILF69WqSPQkoJa9NYvXq1U4nwVFi3X90bJSP/Ogj7D22l96hXhYkL+CaVdfw6l2vhiVQGA/J/Px8du7caU4P3tTUxKZNmyYNHuCfNbaioiJoltZIztgaCSQHCLCf90VVNw0NDaGUIiUxQVyQGBoacjoJjhLr/rsO72Lvsb30DPWg0fQM9bD32F52Hd4VlvMb8xdt27aNyspKc3tlZeWkVVAGxrTf8Yz0uZvs5v2YCBJKKa9SKl8ptVUpFbE/U06ePAnAvCSPuOomw10qse6/r3MfvUO9Qdt6h3ppPh6e6URGRvxL9ubk5AS1G+zcuXPKINDW1jbtanXGOhHGKxYx3KViN+/HSnVTJmCUd4uBiKxtaCygMi/RI64kYbhLJdb9r1pxFQuSF9Az1GNuW5C8gOyLwjO+Y/78+eb7rVu3snPnTjIzM89bSsjMzJx2xbiqqqqYb5OwukvEbt6PWEkiUDooUUoVjdu+1VJqyAHQWjcBp4GtQMUkpwsLxipc85ISxM3dZHcx9Hgn1v1vWXsL16y6hrTkNBSKtOQ0rll1DbesvSUs5+/r6zPfFxcXU1FRMW1VE/gDRU1NTVjS4BRWd4nYzfuRrG7aNH6DUsoLFGita7TWO4EyY5/W2gc04Q8UEcEYeZkisCQhfdRprPt7Ejy8eterPHvHszyU9xDP3vFs2BqtIXisgFFCsK5bPRVPPvkkxcXFE7rExhPSx0nYzfsRq27SWtdM0r5wJ2CddN4XKE1s0lrvCBxTCpRHIk0ZGRmAvyQhrU3CcJdKPPh7EjzclnUbt2XdFvZzjx8rMP7BPxU5OTlUVVVRWFhoVj1t27bNXL86Nzc3qLpp+/btZs+pWEH6OAm7eT/abRJewJozTwOLgYZAsMjHUrqwEqi2Mquu6urqWL9+Pe3t7fT395Obm0tjYyPLli0jOTmZo0ePsmHDBlpaWhgdHWXjxo3s2rWLa6+9lrHhQY6/043P52P//v14PB6ysrI4cOAAq1evZmhoiJMnT5rnTE1NJSMjg4MHD5KRkUF3dzddXV3m/rS0NFauXElLSwtr1qyhq6vLXPO3sbERr9dLeno6ra2tZGVl0dHRQU9Pj7k/PT2dhQsX0t7eHrJTc3OzORVwZ2cn2dnZkzrt27ePpUuXzimnUO7Trl272LBhQ8SdRkZG6O7uZt68eQwODqK1JjU1lf7+frMr5vDwsLlNKUVKSorZl19rzcjICPPnz6evry9of3JyMqOjo4yOjpr7ExISSEpKYnBwkJSUFEZGRoL2ezweEhMTOXv2LAcPHqS3t5exsTH+/M//nL6+PvM7b7/9NsPDwwwODvL2228zf/58uru7SUxM5PLLL+cnP/lJkNPIyAhvv/32pE49PT1RcTL2Dw8PMzY2FrTf4/EwNDTEvHnzOHv2LMnJyeb+xMRElFIMDw/H3H2aqZOR5vM5vfbaa0HPvdmiItk9zGiP0FrvCHwuAXyWzxVAdaDqKZTz6tmku729nYyMDP70h2/wbv8wP/3LD4Z8jnjFcJdKtPytC9DHEsbDRyIS3a350Jr3lVJorUOqf4t2F1gf/tKEwWLO9WqKON3d3QCkJCaIm5bDcJeKdH/Ja3xLdgf7eT/aQeJ5YI3lszfQs2lGKKW2KKV2zPbiXV1dgMxxEoa7VKT7S35QSnYH+3k/Ym0SSql8oADwKqXaAj2afEqp6sA+mKL9YSq01i8BLyml7plNms6Nk5A34jrWxwlEGun+kscKSHaHGB4nEQgKhVrrAq11jWX7zsC+Guv2aGD0F05JkhckYn2cQKSR7i95rIBkd4jtcRIxR1paGuAfcS2tuslwl4p0/4QEUf/Vg5DsDvbzfqxMyzEjlFJbgC2zPX7lypWAv01iYHgUrbWYgTaGu1Sk+0ueCVWyO9jP+3EVYrXWL2mti6b/5uS0tLQA/sF0YxqGR+XMDmm4S0W6/+DgoNNJcAzJ7mA/78dVkLDLmjX+jlXm6nQjctolDHepSPeXNk7AimR3sJ/34ypIhK8LbGCda0GT/EnvAirdf2RkhEWLFlFQUGBO711cXOx0sqKC9KnC7eb9uAoSdqubjLlqUpL8JQlJPZxmMk/PXEa6vzFWoKqqiqqqKqqrq8nNzQ05UEy1gl2kjgsH0sdJ2M37cRUk7GKOk0iSV90kfZxAXPhfdBEoFfy66KKwnHqysQL5+fk0NDSEdJ7pphYP93HhwB0nEaPjJGIRc5xEorzqJunjBOLC/8SJmW2bBePHCvh8PkpLS4NKEuXl5eTm5lJQUIDP56OtrY3c3FxzW2lpKU1NTRQUFFBe7p+o2ViRLjc31/yL1efzmd8pLCyc9Lho4o6TsJf346oLrF2MKY0lliRiffWwSCPd3+Px53njL/qamhoaGxvNKb+bmpqorq6msbGRtrY2SktL8Xq95tTfPp8Pr9dLTU0N1dXV5nmN9zt27KC0tJSKigrz/AUFBVRVVZmfrcdFE8NdKnbzflwFCbvjJNLT0wH/tBzv7t3JrzYMc7btQurr6ykpKQlXMmMSw10q0v0TE/3/1Y3lRgsLC2lrazODRGVlJadPnzaDSFtbG2VlZRQXF1NfX09xcfGkD5u2trZJA4DX642Z/1OGu1Ts5v24qm6y23Dd2upf7yg5MYGUi7L48t0f56abbmLz5s1hS2OsYrhLRbr/+LECZWVllJYGLyW/fft2s2G7sbGR/Px88+E/Wb12U1MThYWFbNq0aUID+OLFi8NsMHukj5Owm/fjKkjYJSsrCwBPgoxR1lYMd6nEhf/y5TPbNgvGjxXIzMwkPz+fHTv8Pcq3bdtmVhWBv4TQ1tZGZmYmZWVlbNq0KajNAaChocFcpW5876XJSh1O9TCTPk7Cbt4XVQ7r6Ohg5cqVeBIUg8dbKPvhTjZnLKa+vp68vDynkxdRDHepxIX/8eMRO/Xw8PCEbaWlpRQUFFBUVEROTo45fgL8DdJer9dsU8jMzMTr9ZKfn8/NN9/Mpk2bKCsrIzc3l+rqarxe73lLD9bjrMEoGgwPD5OcnBzVa8YSdvN+RFemixSzXZmurq6OG2+8kT8cP8v/+s5rfO8zOXx044oIpDD2MNylEi3/WF2Zrru7m4ULFzqdDEeQ6G7Nh9a8P5uV6eKqJGG34dqoV00MVDeNjsVfgJwtcTFOIIJI95c8VkCyOwgbJ2G34droL5yg5AWJuBgnEEGk+0seKyDZHdz1JELC6AqWGJhffkRQkJDeBVS6v+SxApLdQVgXWLsY9ZLGGiRjgoKEtDrZ8Uj3l/yglOwO9vO+qCDR3t4OyCxJGO5Ske4/NDTkdBIcQ7I72M/7ooKE0dpvjJMYjcOeXbMlFnvcRBPp/vPmzXM6CY4h2R3s5/24ChJ215MwIqoZJEblTPAn/S9p6f6SRx1LdgdhJQm7vZv6+/sBa0kiPOmKBwx3qUj3j8fxUOFCsjvYz/txFSTsYvQXNoPEmJyShPRxAtL9JY8VkOwOwsZJ2MXoL3xuMJ2TqYku0scJSPfv6+tj0aJF5toQxqupqWnG54jXdcLdcRLuehIzZtmyZYB1MJ2cKGG4S0W6vzFd9u7du8WtrSF9qnC7eV/Ur2dM8mWUJCR1gZU8wRnEh//jFz1O74neoG0Lli/g/uP32z63UvJmPjaQ7A72876o6qajR48CkBAIEpIG0xnuUokH//EBYqpts2GyWWANfD4fubm5FBcXk5ubG7TOhLGtuLiY06dPhyUt0eZ87hKwm/dFlSQ2bNhgvk9MUKJKElZ3iUj3N8YKGCvPGRiLCjU1NZlVUYsWLaKsrIyamhra2trMOu3nn38+uokOE9LHSdjN+3EVJOzOAtvS0sKSJUsAfw8nSYPprO4Ske5vjBUwli8dj7FeBJxbVa66ujooqMTSanOhMDg4SFJSktPJcAy7eT+uqpvsjpMYHR0133sSFKOCBkpY3SUi3X+6sQJzuTFb+jgJu3k/roKEXTZu3Gi+l1aSsLpLJB78FyxfMKNtsyE1NTXkYwoKCsyV6Xw+X9y2SczGfS5hN+/PuLpJKXUBUA7kaq03K6UuAe7QWj9uKwVRpLm52VyhyZOgRK0nYXWXSDz4h6MX01QYo25zc3ODSg3bt28nPz9/0mPy8/OpqqoiNzeXTZs2kZmZGbH0RZL+/n7RswDbzfuhtElUAV8EygC01m8ppQqAuAkSK1acW6o0UViQsLpLRLp/UlISZ86cmXK/dcBVa2ur+T7a61FHAsntEWA/74dS3ZSutX4LsD5Z47YDcoKSFSRcXFxcZkMoQaJSKfVLIFMp9Qml1Kv4SxdxQ2dnp/leWhdYq7tEpPtLHisg2R3s5/0ZVzdprR9TStUA24Crga9prffZunqUyc7ONt8nJChRg+ms7hKR7i+58VayO9jP+zMuSSil7tda79Nafy3w2qeU+r6tq0eZ/fv3m++llSSs7hKR7i95qnTJ7mA/74dS3VRg/aCUuhDYbOvqUca61q20LrDS1/mV7i95/iLJ7mA/709b3RTo6loFXKWUqudcY7UGamxdPcpkZWWZ76UNprO6S0S6f0pKitNJcAzJ7mA/709bktBav6W13gQ8prXerLXeFHht1lp/zdbVo8yBAwfM956EBFElCau7RKT7DwwMOJ0Ex5DsDvbzfigN144HBLtzN61evdp870lAVBdYq7tEpPtLHisg2R3s5/3ZjLheZNl8Wmv9JVspCAGt9UvAS0qpe2Zz/NDQkPnek5AgKkhY3SUi3V/y/EWS3cF+3g+l4Xo30Ii/TWJH4HNcTQt58uRJ871HySpJWN0lIt1/ZGTEfN/U1ERBQQG5ubkT1o+YDcaypm1tbTG5xKnVPZwY63DEOnbzfijTcpzWWj8Z6CnQpbXerZS6w9bVo4z1hiYmJDAiaPnSeMjMkUS6//z58wF/gCgsLKS6utqci6mmJjz9TzIzM4Om9AiFtra2iM0NZbiHQiTTE23s5v1QShJGr6Ya4F+VUvcDsfdnw3mwzk+TkACCYoTtxdDjHen+fX19ANxzzz1UVFQEPQCnmuAvmoxfDCmcGO6hEMn0RBu7eX/GQUJr/eHAv28B9wDvAnH155l15KW0koT0UadO+t94440TXt/73vcA/wNssv1PP/00AKdOnZp0f2VlJQBHjhyZURqUUvh8Ptra2qYMCj6fj4KCAsrLy82HZEFBgVk15fP5zO9Otqzp+OqX8vJycnNzKSgowOfzTblMamlpqVkFVl5ePiFNUy2tumPHDjN9RmlovIPP5+P666+ntLTUPL68vNx0mozJ0jPZtcYzme9Mfs/pHI1qQSMt468zHbbzvtZ6xi8gG7jJ+grl+HC9/MkOnePHj5vv//QHe/XH/vm/Z3WeeMTqLpFo+f/+97+fsO2GG26Y8Prud7+rtda6t7d30v1PPfWU1lrrd955Z9L9zz33nNZa67fffntG6RoaGtKNjY06MzNzyu+cOXNGA7qsrGzCvoqKCl1UVKS11rq6ulrn5+eb+7xer3l8Tk6O1lrrxsZG8zutra26qKjIPP+ZM2eCjtNam8dNlabxxzQ2NuqtW7cGHX/mzJkJDuOPB3RVVZXWWuv8/Hzd2Ng46XWt6TnftWbiO93veT5HazrOnDkz6XUmw5oPrXk/8OwM6XkbSu+meuA08JY1xgC19sJU9Dh48CDLly8HjPUk5JQkrO4ScdK/rq5uyn3z588/7/4lS5acd//FF188ozQMDAyQmZlJW1ubuW3nzp1UVFRQU1Nj9gDyer2UlJSY32lra6OmpsZcCxtmtqxpZWUlp0+fNr9nXHeyZVKnY7JjKioq2LZtm/md/Px8ampqyM/Pn+CQkZFhHp+ZmWmWpDIzM2e0kNL5rjWd70x+z6kcKysrg67r9XqnvM75sJv3Q2m4PqO1/sisrxQDZGRkmO/9QcK5tEQbq7tEpPsnJyeTkpJCTk6O+YDbunUrW7duZdGic73arQ/upqYm7rnnHp588kkyMzNDXlti+/btbN261fzs8/lmtUzqTI8xHvjjg4/VL5TzzeRaVibznenvGUqaxl9nOuzm/VAarquVUh8PjJeIS7q7u833HiWrJGF1l4h0f2Od4yeffJLi4uIp67KtD6uGhga2bdtGTk5O0F+sM1nWdNu2bUEPwZn8xTuT+nWDwsLCoPNb/7If/8A1SkmhYqTnfNcymMp3Jr/nVGzbts1sezLOOZvf1W7eD6UkkY5/VbrWQDdYhb9+61JbKYgiXV1d5nuPR9YssFZ3iUj3N4JETk4OVVVVFBYWmg/3O++8c9Jj7rzzTnJzc6mursbr9Zp/Fc9kWdOcnBwKCwspKPDPC1pQUEBRUdGU6cvPz+fmm29m06ZNMyqx5Ofn09TUZDY+b9++nczMzJACzXTnt6ZnumvNxHeq33MqcnJyzMZsI01lZWUTrmOtzpoMu3lfzTTKKqUatH8OJ8dRSunZ/HXQ3d1trnX75Wf38dujPuq+mhfu5MUkVneJRMv/4MGDrF+/PuLXCZXR0VGxM+FKdLfmQ2veV0qhtQ5pWtxQq5vi+ilj7S8sbT0J6eMEpPvPZqzAXEGyO0RxnAT+9SR8Sqk3lVL1SqmGQI+nuCEtLc18L21lOqu7RKT7JySE8l99biHZHezn/VDaJG62daXzoJTyAplAPtCktY7IOhUrV64030srSVjdJSLdX/JMqJLdwX7eD2XE9buTvWxd/Rx3Am1a63LA3mxj56GlpcV870lQjAmaHdLqLpFo+s+2N00kGRwcdDoJjiHNfXz+s5v3z1uSUEq9afReUko14B88Z+72p0dPuoRpoHRQBPi01jss27cCPsCLPzA0GfuVUjlAkw2f82KdodIjrCQRi7NzRpNo+SclJdHf3z+rSeUiieTV2aS59/f3B5We7Ob96UoSH7a8vxl/dZDxMj5PxYSeUIHAUaC1rtFa78TfpdbKNq11xEoSQV1gE5SoqcKldwGNlno8PKEAACAASURBVP+yZcs4duwYfX19MVWiiNR02fGAFHetNX19fRw7doxly5aZ2+3m/fOWJLR/Mj+De7TWj1v3K6W+D0y66JDWukYpNb4D9Z2AdS5hn1IqR2vdFChhfFsplam1nn6EyCyw9mv2D6aLnf/EkSZc/cfjlWj5X3CBf6xpR0cHw8PDUbnmTBgYGGDevHlOJ8MRJLknJSWxfPlyMx+C/bwfSsN1AWAGCaXUhcCkVU3nwYu/qsngNLBYKZUPFAPbgDYi1C5hnfVR2mA66espRNP/ggsuCPpPGgtIHicj2R3s5/1pg4RS6hKgCsgJdHk1BmJo/GtL2MUbqHo677mUUkX42zgA/6Rp69evp729nf7+fnJzc2lsbGTZsmUkJydz9OhRNmzYQEtLC6Ojo2zcuJEXX3yRa6+9FoBjR44wNqZ57bXX8Hg8ZGVlceDAAVavXs3Q0BAnT540z5mamkpGRgYHDx4kIyOD7u5uurq6zP1paWmsXLmSlpYW1qxZQ1dXlzn9b2NjI16vl/T0dFpbW8nKyqKjo4Oenh5zf3p6OgsXLqS9vT1kp+bmZlasWAFAZ2cn2dnZ7N+/f4LTvn37WLp06ZxyCuU+vfDCC2zYsGFOOYVyn3bv3s2HP/zhOeU00/tUU1PDqlWr5pTTbJ97syGUEdePaq2/Zvl84XS9mwIPdiwN00X4g0J54HMV8G2tdUiN1bMdcd3c3Ex2djYA/+eXh/jH2sO0P3pryOeJR6zuEnH95fpLdodg/0iPuL5KKXWBUuoSpVQX8HygTSIUnid4NTtvqAHCDunp6eb7hAT/7yRlQJ3VXSKuv1x/ye5g3z+UILFYa30Wf9vBk4Fpw6fsWxVoZygACgPv0Vr78E/vkR/YNr5303lRSm1RSu2Y/puTY11/NzEQJKS0S8x27eG5gusv11+yO9j3D6XhWimlPgFs5fxdXwF/7yYmaWcItD/MCq31S8BLSql7ZnN8VlaW+d4sScRQN8VIYnWXiOsv11+yO9j3D6UkUYi/N1OR1rrd0qAdN3R0dJjvpZUkrO4Scf3l+kt2B/v+oQSJLmARwVVEF9q6eojYrW7q6ekx33sCk35JGSthdZeI6y/XX7I72PcPJUhU4Q8Qb4E50K7A1tVDRGv9ktZ66pVLpiFonESgfV9KkHDHSbj+UpHsDvb9QwkS6YHAMH7+prjBOq+6x+NXHxGyhKn09RRcf7n+kt0huutJVCqlfglkKqU+oZR6FX+X1rjB2hXMo4wusE6lJrq43QBdf6lIdgf7/jPu3aS1fkwpVYN/6oyrga9prffZunqIKKW2AFtme7x1aP65hmsZUULytATg+kv2l+wO9v1DWrJJa71Pa/21wCuqASJwfVttEu3t7eb7c4PpbCcrLrC6S8T1b3c6CY4h2R3s+4ta18+6QL20koTVXSKuv1x/ye5g319UkJi0JCFkMJ3711S700lwFMn+kt0hwiWJQOO08d7xGbLsjpPo7+8330sbTGd1l4jrL9dfsjvY95+uJKGUUu8NvA9pnqVIENZxEoEg4Y6TkIHrL9dfsjtEfpxEKfCkUuowUKCUelMpddj6r62rR5mgcRJKVpBw+4q7/lKR7A72/adbvnQfgXWulVJf1Vo/ZutqDmNd99XjkVXdZHWXiOsv11+yO9j3D3WcxAX4Z4BdDFRrrf9o6+pRJjk52Xx/bjCdjCBhdZeI6y/XX7I72Pefce8mpdTNQC3+mWDXAi8opT5u6+ohYrfh+ujRo+Z7aQ3XVneJuP5y/SW7g33/UNaTKNFab7JuCKx5/R+2UhACdteT2LBhg/le2sp0VneJuP5y/SW7g33/UMZJTDaZX1xN8NfS0mK+l1aSsLpLxPWX6y/ZHez7h1KSqFJKVQKVgc/FwHO2rh5lRkdHzfdGSWJUyGA6q7tEXH+5/pLdwb5/KA3XTyqlGvAHB4ByrfVuW1ePMhs3bjTfGyWJ0VEZQcLqLhHXX66/ZHew7z+bCf6+GHjFVYAAaG5uNt97hJUkrO4Scf3l+kt2B/v+ouZuWrFihfle2ohrq7tEXH+5/pLdwb5/KG0SjmN3PQkr0hquXVxcXGZDXJUk7M7d1NnZab5PEDaYzuouEddfrr9kd7DvH1JJIjAT7GLrNq11ra0URJHs7HMT2SYmGGtcywgSVneJuP5y/SW7g33/UEZc1+OfCfZOy6vQ1tWjzP79+833gRghpiRhdZeI6y/XX7I72PcPpSRxRmv9EVtXcxiPx2O+l1aSsLpLxPWX6y/ZHez7h9ImUa2U+nhgkr+4JCsry3xvlCSkdIG1ukvE9ZfrL9kd7PuHEiTSgReAxnhdT+LAgQPme6MkMToqY41rq7tEXH+5/pLdwb5/KNVN+VrruOoNNZ7Vq1eb788NpnMqNdHF6i4R11+uv2R3sO8fSpCoVkot1Fp327qiDeyOkxgaGjLfnxtMJ6MkYXWXiOsv11+yO9j3D6VkUAD4AlVN9UqphkCPp6hhd5zEyZMnzffSBtNZ3SXi+sv1l+wO9v1DKUncbOtKMYB1QXBpg+ncxeBdf6lIdgf7/qGUJHZord8d/7J19ShjXRBcWknCXQze9ZeKZHew7x9KkGhQSt1n62oOk5qaar6XtjKd1V0irr9cf8nuYN8/lOqmAiBfKfUAcBp4F9Ba6822UhBFMjIygj4nJigxJYnx7tJw/TOcToJjSHYH+/6hBIm4moJjMg4ePMjy5cv9Hy66iMMnTsC3LV9YvhyOH3ckbZEmyF0grr9cf8nuYN9/xtVNgfYHjb8BuxBYFG9tEkER9cSJiV+YbNscwf1rKsPpJDiKZH/J7mDfP5QJ/m4GaoHNwFpgp1Lq47auHmW6ux0b4uE4kt3B9ZfsL9kd7PuHUt1UorXeZN0QGCfxH7ZSEEW6urqm/c6PX/w5F10wj4aGBkpKSqKQqugwE/e5jOsv11+yO9j3V3qGE9wppX6ptf7wuG0N4wNHNFBK6Zmm20p3dzcLFy40TjL5uQP/lj32GIyNzZlAEeQuENdfrr9kdwj2V0qhtZ784TcFoXSBrVJKVSqlPhF4vQo8F8rF7KKU2qKU2jHb44P6C0/SkGNtsi796lfxDYxSXl4+28vFFG5fcddfKpLdIYrjJLTWTwKPAh8OvMq11o/bunqI2J2WIy0t7dyH48dBa/bU1qLwlyBWAF/60pfMr3z77x7gXypf5tbCu3nx56/GdcAIcheI6y/XX7I72Pc/b3XTTNaO0FqftZWCWTDb6qaOjg5WrlwZtK28vJzW1lbWrVvHoUOHOHLkCPn5+dx338Rxg1d+uJDBzlY+cHUOd33m07zxxhvcd/9XSfTE/uS4k7lLwvWX6y/ZHYL9Z1PdNF2QOIy/26v1pNYDMrXWUV/2abZBoq6ujhtvvHHa75WXl5OYmDhpoLCSvHIdyQsuJC9nHR/K2RDT7RczdZ+ruP5y/SW7Q7B/2IPEpAcodSFQBBQDNVrrL4Z0gjAw2yBx5MgRLr744hl9t7y8nM2bN/Pcc8/R0dHByy+/fN7vZ+ZcR17eTYz5jnFZ1qWUlJTw4EPf5KqrNrE4LZn6+npHg0go7nMR11+uv2R3CPaPaJBQSmUApUA+sBN41KnBdLMNEs3NzWRnZ4d8nLVKqrKykmXLlp03aKy/6Q56jr3J6dF59B5+A4Da2lqu/dD1fPTP72feiiy+cF0mh3//m6gFjtm6zxVcf7n+kt0h2H82QWLacRJKqWzgASAHKNNaf2maQ2IWn883q+OsD/J7772X8vJyVq5cybFjx1CBrrTWoHGw9oUJ5/hY8dcY7X0Xz7JMep75DrvK4e8fKaO8vDwqgWK27nMF11+uv2R3sO8/XZuEsahQqda61taVwkhYxkmEEaOk4fF4+P73vx/Ssbt37+amm24Ke5rG4/YVd/2l+kt2h8iPk1gLrME/BUfXuNdppVRcDWWMVH/pkpISKioqyMjIoKioiCeeeIKrr77afH8+Hv3RL/jZz3/JzR/+Xzzw3Wf54fMvUVZWFvY0un3FXX+pSHYH+/7nrW7SWi+ydfYYw+v1RvT846ulwF/KqK2t5bnnnqO5uZlt27ZRWVnJFVdcyR9HvVQ/9RjVTz3G/LXXUPtXnwbg5s/ez+ba2rBODRJp91jH9ZfrL9kd7PuH3LspFohG76ZosGfPnvNWNf3Jpz7LkgVJZF261nawmMq9891+0hekkJwY+2M97BBr9z7aSPaX7A72ezfN7SfDOFpbW51OQhD19fUTqqO++MVzPYp/+uzT/OBfn+S7P36J9MuvZenl17Jl25+Stel6nnnhFfbs2WOOAh8ZHeP24hIe+O6zQdsNxrt/69Ey7n74h2Td8AnyPv7pSY+ZS8TavY82kv0lu4N9f1EliVgceRnKiG+DtOxb6GneBcCn//RzeC79EHW7XuSdE50MtDYA/i63eXl55jHj3e/9zo/4h7+5O+i844+ZS8TivY8mkv0lu4P9EdehTBUe98RiZpmsGqm8vJyioiLWrVvHnj17JozJMAIEwM9eP0jPvz913ms8/K1HaR+Yx+8bfs2h3+9n4/UfZd/e18IjECfE4r2PJpL9JbuDfX9RQaKnp8fpJMwIa+AYGRkxx2R0dXXx+uuvA/DJP/tznvu3H9DT8vqE49M33cZdxf8f85IS+NQnt/FvO1/h6G//x9z/X88cBGDr3Z+nvr6BUykrePbR+6ivr5+zJYl4ufeRQrK/ZHew7x8T1U1KKS/+kdyLtdbTTgUea+MkooUxVQjAc889x44d536qW2+9lVWrVnHB8vfw+MMPzvicTzz9Av94MIXKovdzTWZ62NMcK8T7vbeLZH/J7hDd9SQihtbaBzQBEe2rFu/9pUtKSsjLyyMvL481a9aY4zCKioq4/vrrqaio4KN5H5jy+FtvvZWrr746aDr09y5eAMAfjk+9xGHx/X/LZXkf51N//SDFxcVx2cAd7/feLpL9JbtDhMdJ2CFQOigCfNbSgVJqK+DDHxDatNZNkUrDeNLT585fylN1ia2vrw8al5Gbm2uOAr/vvvvIy8szx24AvPHGGyyafxUHOyef8b3xj6f56bFUTtS9yKG6FwHMY8dzsnuAtJRE5ifHXi3mXLr3s0Gyv2R3sO8fyf/NE5Y1DQSOAq11ceBzNVAQwTQEIaHIaQQPo23BCAjHjx832xysASYvL4/nC/+anW9dzHsG36L197/hrVO93JL3IdYsXcDf/PMLXOi9hBPTXHdsTLP5zi9z9ebN/EXeWsdnvR2PhHt/PiT7S3YH+/4RCxJa6xqlVOa4zXcC1k67PqVUTrRKE+3t7WRkZETjUjGD8aCuq6vjU5/61KTfuf8zt/Dpj9/KXz4NqZmb0UrzHxX+KqVFeX/OlaNNJGfncmxRNpsX9U/awP37zrMMei9h58NF7Hx46tKGU0i891Yk+0t2B/v+0a4X8OKvajI4DSwOvM8HNiulMrXWbeMPVEoV4a++AvwPvfXr19Pe3k5/fz+5ubk0NjaybNkykpOTOXr0KBs2bKClpYXR0VE2btxIT08Phw4dAqCzs5Ps7Gz279+Px+MhKyuLAwcOsHr1aoaGhjh58qR5ztTUVDIyMjh48CAZGRl0d3fT1dVl7k9LS2PlypW0tLSwZs0aurq68Pl85n6v10t6ejqtra1kZWXR0dFBT0+PuT89PZ2FCxfS3t4eslNzczMrVqyY1iklJYW6urpJneg/N+N7f1t90O9+Zs8PqAP+5V8q+M6RVVyxcQVXLz9Lc3NzkNOze94MOq6trY1LLrkkok6h3Keenh4aGhpi/j5FKu/19PTQ0dExp5xmep9GRkaC8v5ccJrtc282RLR3U+DBjtEmoZQqwdJGoZSqAKq11jtDPO+sejft3buXa665JuTj5gLnc5/JSny1tbV89fs/YcCbwf/esoHWg8FrYXxqx+s0vfQ07544yqpL1rLJ28+aNWtipspJ8r0H2f6S3SHYPx4G0xkN1gaLgQmlhkjR398frUvFHOdzLykpCZqIEGDdunVm0HjiiSeor6/nzltupPTzW9n6JLznyg+xe+9v+fyffob/+p/XqW44wu0F13Oy/Q+8tKOMJmKryknyvQfZ/pLdwb5/tEsSXvwLF5kN11rrGTdcK6W2AFuAeySOk7BDqO7WMRlGI/RUExKmZd8CvV30vPlG0PZYmuZD8r0H2f6S3cH+OImIBQmlVD7+dbCNwFAT2G50gQX8DdyzOPesqpskL4geDveZVEsBXHzFB9ErN5B30TAb1q+bUOX0wN89wuu/+T1ZWesY8x1j7Vr7s9xOh+R7D7L9JbtDsH9MVTcFHv4TAkCo7Q/hZNmyZU5d2nHC4T6+WqqjoyNoXqknnniCq666iv9b8RQ/razgGWDp+65l9x9OsXSsiysuX8cXv/w3/Mexefzhp8+yJ3BcbW0to2Oj7Dq8i32d+7hqxVXcsvYWPAke22k2kHzvQba/ZHew7x97o57Og6W6aVYkJyeHMTXxRbjcreMwjLW+jRlsR0ZGzOqln1Y+A8A7v/s1v/zdrwGouWYL3/7e0wwkLgg65z8+/wvu++19/M73O4YZZkHyAq5ZdQ2v3vVq2AKF5HsPsv0lu4N9/7gKElrrl4CXlFL3zOb4o0ePsnbt2jCnKj6IhPt0o7737dsXVDV1Yu9LQd8r/uIXeXXv73ixthwWASn+7T1DPew9tpddh3dxW9ZtYUmr5HsPsv0lu4N9/5iY4C9UZtsmcerUKZYsWRKBFMU+Trgba2V4PB5zahArtbW1jI6OUfBwPtxI0ExiCsVDeQ/x4PXnJivUWvP1v/8ma953JZlL00Ia1S353oNsf8nuEOwfU20SsUhLS4vYzOKEu/EAt66PUVlZSXZ2Np/85Cepr/cP3Pv8Rz/PD9/9oVmSAGAkgScff56d22s53dWFAvoSFvDuu10Md3wDCK2LreR7D7L9JbuDff+4ChJ22yRGR0fDmJr4wkl361/79957r/neaL94tOxRLp1/KW/2vQlJkEASY28P8/bL+3k7TAVdyfceZPtLdgf7/qKqm3w+H15vRGcjj1li3f3RskcZWzPG4Z7DdDR18Oo/vwrnucUL1m7mEx+6gs/96WdmVO0U6/6RRrK/ZHcI9o+pcRKRxB0nETrx5G60ZRw7dgylFCdPngRg6dKlKKWY713CrgPH6W7+BQDb7v48F8zzMG/xSl47fIr1G6/iC9ddQkNDQ9AEh/HiHwkk+0t2B/vjJEQFiUOHDrFu3boIpCj2mWvu/1r5Evd88mNB21IzN5OQoOg97B/5/Tdff5iRkRHKHvoGb791eE75h8pcu/+hINkdgv3nfMO13TYJl7nD6T8e5IknngjqYjt+Btt/+OY3SM3czC8OvkNWwgmuv/Zqiv7qK3jny+437+ISCjGxfOlM0Vq/pLUumv6bk9PZ2RnO5MQVc829pKSEkZERioqKgpZjHU9/Wz1v/ux7vPLiC/z7L+tZtnYjH9zySfbs2RO0DOvn/+ZByp96YcL2ucJcu/+hINkd7PuLqm6S3IA1l92NyQiNGWwNrDPZTsa6jTksXbqEi1et4pX9nZwNtHHE0sSE4WIu3//pkOwObsN1SLz22mtcd911EUhR7CPRfbrBfFa8ubfia3wFgG888ihpSSpm1sIIBxLvv4Fkdwj2n/NtEnbxeMI3YVy8IdHdOpjvYx/7GDfccAOVlZUsXbqUV155Jei7RoAAePjBr/HyL3553nNrrVFq8v9rDz3ybTZk57BoQXLMrPUt8f4bSHYH+/6iShKSh+dLdodg//FdbEfHNLt+Hhw0UjM3c8H8JDh7nItWv5du32muu2YTf3b3Zyj520foW/dRvnbLZXS2/m5CEMi795+p+4e/BmKn6kry/ZfsDvan5YirIGF30SHJ/aUlu8P5/a1tGlprktJX871H/27Kc6VlvZ+eltcByPnwVt4+3Yd3xXu5btkwaUtX8Xx7CieeewCInSAh+f5Ldgd3nERIHD58WOxskJLdITT/qVbgm4y07Fvoad5lfs66/nY6zw6QuGgV1yweIO/qK2Kiukny/ZfsDsH+bpvENAwNDTmdBMeQ7A6h+dfX11NUVMSxY8dobW1l7dq1nDx5Eu/idH75i3MBYfk1WyZMf95xdpCeQC+p5Y/8kJKSz/GVB/43115zNcsumOdYG4Xk+y/ZHez7iwoSJ0+e5PLLL3c6GY4g2R1C85/qIV5eXk7Gey42F1nSWvPkXv++Oz7zOXbtPRBUqvjD8bMcPtnNzrdT+L/f/igQ2sy14UTy/ZfsDvb9RVU3SV4QXbI7RMbfaMsAf+njypwc/ldBAQDrr76RI4MppKUkcvb0SfoO+0eDO9VGIfn+S3aHYP/ZVDfF1YhruzQ2NjqdBMeQ7A6R8S8pKSEvL4+8vDxKSkr4TVMTtbW11NbWsunyNfT85lWOv/GKGSCSV67j019+kI8W3sUDf/8IA8Mzn8J5cGSUUz2DzPaPOsn3X7I72PePq+omu3M3paamhjE18YVkd4iO//hqqmee/kHQ56GOQxzvgF0HfkVd02b+6ccvsWxhCqN975J95ZX81Rf+jKamxgnn+fLX/p4XjqRw9nf/RfbFF/LQvcUht21Ivv+S3cG+v6jqphMnTrB8+fIIpCj2kewO0fcfP1XIdFOEGFy6IYeF3kW88847AHxy251UvfQL2vf9d9D3Qq22knz/JbtDsL/bu2kaDh48KDazSHaH6Psbf+UbD3LrEq579uzh5ZdfnvS4Nw80BX1+7KHfhiU9ku+/ZHew7y8qSGRkZDidBMeQ7A7O+1urhkZGRli5cmXQokrLli2bMnCAf2Gltw4d4GDnWZZdmccz/+/HIVU5Oe3vJJLdwb6/qCDR3d3tdBIcQ7I7xJb/ZA/28vLyCYEDYOkll/FK5b9T+cwPqa2t5cCxd/ny3R+nFSh/7HHKy8snnK/4/r9lzfuuJPc9i2hs9K/OF0v+0UayO9j3FxUkurq6nE6CY0h2h9j3n6pEsGfPHl6p/Hfz84ZVF5475qv3s27zDRw+1U//O2+z4bIs7ir+Mq90zufYE1sBeOxxfyC5+uqrIysQw8T6vY80dv1FNVxL7i8t2R3i13/8WAzwz+p5//33T/ju8qu3kJqUwOm+Qf/aGJcCF8E3//qb/EXBX+C9UOaaCvF678OF3XESooKE5Im+JLvD3PI3Ase+ffsm7TGVtfk6evPbOMYxSILUxFTWX7CeN/76DTwJ8qbNnkv3fjbYneAvrqqb7I6TSEtLC2Nq4gvJ7jC3/I2qKWOOqfGLKrW8+5r/TYr/n/6xfvad+g1b//6zLD2eytq1a2Ni0sFoMZfu/Wyw6y+qJNHR0cHKlSsjkKLYR7I7zG1/Y30Ms3vt2ZfhRoLnUxgD6oD/gozs61i/9r0MjoySmJBA6V9+noaGhlkHji/c9w1ycjexfsUFMbPIkpW5fO9ngtV/zpck7NLS0iI2s0h2h7ntP7577bWZ1/LIwUfoH+s/96Vh4Lj/bXvza7Q3v2bu+uVP/h9XfCCPin/7MVtu30rfO2/PuLShtea/Tl/ADz5zOwDVu6t5ueVl9nXu46oVV3HL2lscr+Kay/d+Jtj1F1WSOHLkCBdffHEEUhT7SHYHWf6jY6N85EcfYe+xvfQO9ZJEEot6F3F7z+1UfL9iRudY8f6P8f7MRbz/ysu5//6v0j0wwoXzkyZ87+2uPq7+q//rX2RJwVX/cBVv9r1J71AvC5IXcM2qa3j1rlcdDRSS7v1kWP3dksQ0dHV1ic0skt1Blr8nwcOrd73KrsO7aD7eTPZF2azoXcHu6t3U1tby3HPP0dHRcd7Be52v/4z/eB1a2/P4P//+U7oHh/nwpvVce9X7gkoY+46cYfB4C8s/+S0+/eEBvvv2owzhX7+gZ6iHvcf2suvwLm7Lui3i3lMh6d5Phl1/UUHC5/M5nQTHkOwO8vw9CR5uy7rNfDjX1dUFTRVSXl7Ovffea84tBdDc3Exubm5QI/hvf7XHfP/i4Xr2/eFD/LbjLO+M1aOXa9Iv/hMufP/HQXvomLeHIYaD0tE71Evz8WZHg4S0ez8eu/6iqpsk95eW7A6u/0z9rY3gtbW1vPLKK8FfUMDdwCogGdRYIktTr2Rx//+m681/5J2sOkg4txJaWnIaz97xrKNBwr339sZJiCpJNDY2iu0vLdkdXP+Z+o9vBF+1apU5VQjAyy0v+wNEoHut9oxweugAav/DnD0wAslDZgCZn+Rvk7hl7S3hFwoB997b8xcVJLxemSNOQbY7uP6z8R/fu6m8vJzjWcdpSG4I2j4yNsiJ03vhMNAKXAoLCvLZdt0dfOdPPmc2Wo+OjbLr8K6o93xy7709f1FBIj093ekkOIZkd3D9w+FfUlLC5S2Xc8ezd5iN00BQ91o0PFH8BI/+6/M8/98/5WdPNjL/6Bvc+MFc9l/RzO98v2OY4aj2fHLvvT1/UcuXtra2Op0Ex5DsDq5/uPxvWXsL111yHWnJaSgUySRz1fKr+MKNX6CoqIja2lpGRkb4h2/+LT2/+QWndv8rbx/6Lf/+30+x78Q+hhhCo+kZ6uH1o3v5+Zs/Dzr/6NgoL7e8zMP/+TAvt7zM6NjMl3idCvfe2/MX1XAteeSlZHdw/cPpb1QbGd1rJ6s22rNnDzfddNO5DdczcRS4hhUn389HBt7H+suyuO/++4LGd4SrtOHee0Ejru3O3SQ5s0h2B9c/nP7ju9dOhjGv1Lp166isrERfrKkfrjcbvAEYgs7dr/N0y+tkfOh2HvvFWs586CijnhHAP87itfb/puwnZSS2Jc56ug/33tvzj6vqJq31S1rrotke39PTE87kxBWS3cH1j7Z/SUkJFRUV3Hvvvezdu5dPbPwEK1lJMsmgIXEsEY4Bb/q/3/7fL3JqtJ1RNRJ0nqHRQb7+T18nMTGR8vLyWaXFvff2/EVVN0nuLy3ZHVz/WPC3+tsqXQAAEDdJREFUVlO1/k8rie2JJHmSzg3eywLuILi0MQi8ALTA7t27ufLqD7J4QbLZJXcmxIK7k9gdJxFXJQm7NDY2Op0Ex5DsDq5/LPgb1VQPXv8gT21/iicrniQjI4OioiKeeOIJNns3kz6Y7g8MY/7SRkbKGrO08fH7n2BF1hWkX7KBex98hKKiohmVLmLB3Uns+osqSezfv5+NGzdGIEWxj2R3cP3jxf/RskcZWzPG4Z7D6E7NuoR1jIwl8I0HSif9/o23f5q2gwd438YruPeLn6OpceKU5/HiHims/u7KdNPQ3t5ORkZG+BMUB0h2B9c/nv0n9JQ6D7cU3s3KC+eRdem5qc7b29u5+D0XOzKQLxaw3vs537vJLvH8H8Uukt3B9Y9n//E9pYAJExEa7Kp6BoDM3Ov445kBBk8dQSXAWx98K+xda+MFu/deVEnixIkTLF++PAIpin0ku4PrP9f8rRMRVlZWsmzZsimnPl9029WcubIeks49M2Jh4sFoYb33bkliGtrb2+fUf5RQkOwOrv9c87e2O9x7772Ul5ezcuXKc0u4WgLGmbNvwLgCQ89gD/d+50ES/mQeB37TFHNLroYTu/deVJDo7++f/ktzFMnu4PrPdf/xs9euXLkSj8fjr5I6jn9+qXED+d7c/Rtu/X4BGdfdzr4Dn+eqDZeZ53nkW4+SvOJSct+ziMZJGsPjCbv3XlR1k+T+0pLdwfWX6G9USb3nve+hrLOM/sX9jCSM+LvYHgOeASyPkaXvu5Z1l7yHrxR9jgee+Bda/vNFUPDN57/J6NLRuG3wtjtOQlSQqKurEzuvvGR3cP0l+9fV1XHd9dex6/Au/qnqn7hh3Q28Vf0WxzuOT9mOMW/N1Qy0vQF3w7y18xgcG4zbBm/rvXfbJKZh2bJlTifBMSS7g+sv2X/ZsmXn5pv6eqCheitmO4ZZLWVhoPUN/wjwixMYGBsA/HNJ/c8f/4cvlH2Bp7Y/BTi3RkYo2L33ooJEcnKy00lwDMnu4PpL9p/K3WhnKC8vp7a2lueee46Ojo5zpYuLgMSxoGMGRgao/E0NP1l3DSlJCahb/4hvflfU18gIBbv3Piam5VBKeZVSWwOvzEhd5+jRo5E6dcwj2R1cf8n+07mXlJSQl5dHRUUF1113nTlNyEeu/AiJetzf0cPQv/8oZ1ve4J3h1znp6Ry3Rsbr7Dq8K4I2oWP33sdEm4RSqgTYobX2KaUqtNbF03x/Vm0Sp06dYsmSJbNNZlwj2R1cf8n+s3UfHRtl3TfXcYxjDIwOkKgTuSTpEt782zf9Dd5TrJFx0eFL+EjyTVx+WVZQryinqqas/jHVJqGU8gJFgE9rvcOyfSvgA7xAm9a6CdistTZm6opYSaKlpUXsfxTJ7uD6S/afrbsnwcOhrx8KWmDpwH8c4K173uLYsWMcTjjMoeFDE7rWHq9/i39r+QHL3vcB/vPN03i6O9h8xWX85+rakEZ9hyuo2L33kWyT2DR+QyBwFBglBaVUNVAQwTQEMTpqfynEeEWyO7j+kv3tuI9fYOm20nMjtB8te5Tn5z3PwbMHGRgdgCGC1sg4+btf8fPf/QqAXx6/lMEPHTZHffcM9bD32F52Hd416ajv0bFRPvKjj/Dro6/TP9xnq73D7r2PWJDQWtdM0r5wJ2BdcNWnlMoB6pVSXq21D2ibyfnHd+e78847+Yu/+Av6+vr46Ec/OuH7n/3sZ7n99ts5deoUW7dunbD/S1/6Etu2bePIkSPcfffdE/bfd999bNmyhUOHDlFcPLE27MEHHyQ/P5/m5ma+8pWvTNj/rW99iw984AP86le/4oEHHpiw/zvf+Q7Z2dnU1NTwyCOPTNhfUVHBunXreOmll3jiiScm7H/mmWe4+OKLqaysnHROmx/84AcAPP300zz99NMT9v/85z9n/vz5fO973+P555+fsL+urg6Axx9/fEK3wdTUVHbt8tfDPvzww+zevTtof3p6Oi+88AIA27dv59e//nXQ/tWrV/OjH/0IgK985Ss0NzcH7c/KymLHDn9htKioiJaWlqD92dnZfOc73wHgrrvumlAHe+2111Ja6p9F9I477qCrqyto/80338w3vvENAG655ZYJg49uu+027r//fmBivoOZ5b3Pfvazjua9jRs3Opb3du7cyZIlSxzLe0bej0Teu+7K63joLx/in6r+if3P7WfwnUFY5N93+vRp83uD+k3/uhiWrNVDD1/4WRE//uaPaGhooLa2lmNnjtEz2INGc+TdI4xdOgYf9AeVPX+/h+zvZpOemm6ew+5zbyZEu3eTF39Vk8FpYDGwA7hTKXUaqJjsQKVUEf7qKwB8Ph/z589nYGCAsbExBgYGqKurY+HChfT39zM4OMiCBQvo7+9Ha01fXx8vvvgil112GX19fQwNDZGWlkZvby9KKc6ePUtdXR0ej8fcv3DhQrq7u0lISMDn81FXV8fo6Ci9vb0MDw+b+z0eD6dOnaKuro6BgQF6enoYGRkx9ycmJnLixAnq6up499136e7uZnR01NyflJRER0cHPp+PM2fOcPbsWcbGxsz9ycnJ/PGPf6Szs5OzZ89y9uxZtNYsWLCAnp4ekpOTaW1tpbW1ld7eXt59912UUqSmptLb20tKSgp1dXUcOXKEgYEBfD4fCQkJzJs3j76+PubNm8eBAwfo6+sz93s8HlJSUujr6yM1NZXm5mZ8Ph+Dg4P4fD4SExNJSkqiv7+fhIQEGhoa6OnpMfcnJSXh8XgYGBggLS2NvXv30t/fz9DQED6fj+Rk/8Ixg4ODLF26lF/96leMjo6axxs9MoaGhhgaGuK1114zz+fz+UhJSUFrbe6vq6sjNTXV3D9v3jxGR0cZHh5maGiIF198kQ0bNpj7U1NTGR4eZmRkhMHBQerq6vB6veb++fPnMzg4aKaprq6O9PR0BgYGGBgYCDnv1dXVmb+3E3mvpaWFRYsWOZL3Dh06xIEDBxzLe6+88gqrVq2KSN4bHh4mrSONh/If4rF9j3H48GHmzZvH0aNHWbx4MatXr+a3v/2tf9T3+JYADSfe7OTmm28m/ZLLOD16GM2I/3uKoIF+AGNjYxw9c5SFnoWzfu7Nhog2XAce7BhtEoEGap/lcwVQrbXeGeJ5Z9VwfejQIdatWxfycXMBye7g+kv2d9K9vLyczZs38+PnfkxlciWDSwYZ0kPnqqaMUd+TrcqnmRBYEknkhktuCKnayeofUw3XU2A0WBssZobVSy4uLi7xhtG7KS8vj38Z+5dzo7433MBbXW9x/NbAqO+LgKSJxysS0HqM+x+/n7TeNHP7I3/2CAuWL+D+4/dH3CHa4ySeB9ZYPnsDvZtmhFJqi1Jqx/TfnJzOzs7ZHhr3SHYH11+yf6y4G43gr379VR7Y+gBPVjxpjsvYtHoTHj2uZDAE+q0xUAQFCIPeE70zuq5d/4hVNyml8oFi/CWHMq11TWC70QUW8Ddwz+Lcs6pu8vl8eL3e6b84B5HsDq6/ZP94cDd6M+09tpeeoR6SSWaobQheh8RPJvLgQw9Oetzf6b+b9txW/9lUN0WsJKG1rtFaF2qtC6yBQGu9M7CvZjYBwg779++P5uViCsnu4PpL9o8Hd0+Ch1fvepVn73iWh/Me5tPJn6b6z6qp/n417018r61z2/UXNXeTxxM786lEG8nu4PpL9o8X96AxGdef237oxkM88rcTuybP+Lw2/WNi7qaZYrdNIisrK5zJiSsku4PrL9k/3t09CR4WLF8wYftk2ybDrn9MzN0UKu56EqEj2R1cf8n+kt3B/noScVWSsMvq1audToJjSHYH11+yv2R3sO8fV20SSqktwJbZHj80NBTG1MQXkt3B9ZfsL9kd7PvHVUlCa/2S1rpo+m9OzsmTJ8OZnLhCsju4/pL9JbuDfX9RbRISF4M3kOwOrr9kf8nuEOzvtklMQ2Njo9NJcAzJ7uD6S/aX7A72/UUFidTUVKeT4BiS3cH1l+wv2R3s+4tquM7IyAhfYuIMye7g+kv2l+wO9v3jqiRht+H64MGD4UxOXCHZHVx/yf6S3cG+f1yVJKwoFVLbi4uLi4vLLIjL3k2zRSnVoLWesPa2BCS7g+sv2V+yO9j3j6vqJhcXFxeX6OIGCRcXFxeXKZEWJGY9g+wcQLI7uP6S/SW7g01/UW0SLi4uLi6hIa0k4eLi4uISAnHbBTYULOtqe4E2rXWTw0mKOEqpCqAi8DFfa10e2D7nfgullBcoAnxa6x2W7ZO6zrXf4Dz+kvLAVmAxkAtUGUsjS8gD53EPz/3XWs/pV+CHqLB8rnY6TVHyrgZax7nPyd8CyAdKgKLpXOfibzCZv6Q8AOQAOZbPZ6Tkgancw3n/JVQ33Yn/hzLwKaVynEpMFCnD/5dFmWXbnPwttP8vJ9+4zVO5zrnfYAp/kJMHFgPFls+np7nXc+k3mModwnT/JQQJL8H/gU7j/2HnOpn4Pb1KKSOTSPotpnKV9BuIyANa6xqttfVBuVj7q1DmfB44jzuE6f6LaJOYBK/TCYg0Orhu+kmlVOYUX53zv4WFqVzn5G8gMQ8EHoaF5/nKnM0D493Ddf8llCSMBhqDxUCbQ2mJCkqprUqpEsum04F/Jf0WU7mK+A0k5oFAg2xloPoNBOWB8e7hvP8SgsTzwBrLZ6+O454MM6QNqLF8Xqy1bkPWbzGVq5TfQFQeCNSrN2mtm5RSmYG/mkXkgSncw3b/RQyms3T5AsyGvjlNwBn89ZI7AxlkTv4WSql8/I13XqBMT+z+CJxznWu/wTT+MMfzQOAhuZtzfy0v1lovCuyb03lgBu5g8/6LCBIuLi4uLrNDQnWTi4uLi8sscYOEi4uLi8uUuEHCxcXFxWVK3CDh4uLi4jIlbpBwcXFxcZkSN0i4iEYpdSYwi2okr5GplGqd/psuLrGHGyRcXAKcZ9oCW+fSWrdprdec7/suLrGKGyRcXM5RFaPncnFxDDdIuLhgTo6Wo5SqNua8UUqVKKUaA9u8gVd1YHtV4DvVgVejUW01/lyB4xot1yqyHJcf2OYNnKMi8G/ZxFS6uEQfd8S1i2iUUmeAS7TWPqVUo9Y6N7A9B/8UFwWBqqPSwOsMUKoDq3xZzlME5BrTNo87lxfYrbXODZx3u9a60PgecHPgNGeARYG0nDGmV3BxcRKpU4W7uEzHNmCxUWLAP/8N+JcINQNEIIDkAwUzPG8xUGn5XBM4vgb/UpLGnDqnxx/o4uIEbpBwcZmab2utdxofAiWC05bPOcCTwD34Z90snnCGmWEs+jLZ6nIuLo7itkm4uFiwdIetxPLQt/RWsj7IN+Gfw7+JcyWNyc5lpYrgYGKUIlxcYhK3JOHico4aYLdSqkFrXayUqlJKVQf2VQM7xn3/eaBRKVWAP3icnuxc+NsyAP+0zEqpHEtD9re11m2RHqvh4jJb3IZrFxcXF5cpcaubXFxcXFymxA0SLi4uLi5T4gYJFxcXF5cpcYOEi4uLi8uUuEHCxcXFxWVK3CDx/7dXBwIAAAAAgvytJ9igJAJgSQKAJQkAVgg3DzTLVk6SAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x360 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "log = df.get_log()\n",
    "fig, ax = plt.subplots(figsize=(6, 5))\n",
    "ax = plot_mice(log, ax, 'iteration', 'grad_norm', style='semilogy')\n",
    "ax.axhline(df.stop_crit_norm, ls='--', c='k', label='Gradient norm tolerance')\n",
    "ax.set_xlabel('Iteration')\n",
    "ax.set_ylabel('Norm of estimate')\n",
    "ax.legend()"
   ]
  }
 ],
 "metadata": {
  "colab": {
   "collapsed_sections": [],
   "name": "getting_started.ipynb",
   "provenance": []
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
