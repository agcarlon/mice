Setting up MICE
===============

The MICE estimator can be easily installed using pip:

.. code-block::

    pip install mice
